/// cycle(value, min, max)
// Returns value put into given range
var v, d;
d = argument2 - argument1
v = (argument0 - argument1) mod d
if (v < 0) v += d
return argument1 + v